﻿using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModels.ActionPanel;
using WpfDemo.ViewModels.DisplayPanel;

namespace WpfDemo.ViewModels.MainWindow
{
	internal interface IMainWindowViewModel : IViewModel
	{
		IActionPanelViewModel  ActionPanelViewModel  { get; }
		IDisplayPanelViewModel DisplayPanelViewModel { get; }
	}
}