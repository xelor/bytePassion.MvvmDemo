using System.Windows.Input;
using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemo.ViewModels.ActionPanel
{
	internal interface IActionPanelViewModel : IViewModel
	{
		ICommand SayHalloWorld { get; }
	}
}
