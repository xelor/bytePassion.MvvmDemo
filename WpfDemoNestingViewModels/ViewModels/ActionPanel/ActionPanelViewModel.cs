﻿using System.ComponentModel;
using System.Windows.Input;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.ViewModelBase;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.ActionPanel
{
	internal class ActionPanelViewModel : ViewModel, IActionPanelViewModel
	{
		public ActionPanelViewModel ()
		{
			SayHalloWorld = new Command(DoSayHalloWorld);
		}

		private void DoSayHalloWorld () { }

		public ICommand SayHalloWorld { get; }

		protected override void CleanUp () { }
		public override event PropertyChangedEventHandler PropertyChanged;
	}
}
