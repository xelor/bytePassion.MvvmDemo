﻿using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal interface IDisplayPanelViewModel : IViewModel
	{
		string LabelText { get; }
	}
}