﻿using System.Windows;
using WpfDemo.ViewModels.ActionPanel;
using WpfDemo.ViewModels.DisplayPanel;
using WpfDemo.ViewModels.MainWindow;

namespace WpfDemo
{
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			///////////////////////////////////////////////////////////////////////////////////////////////////
			////////                                                                                 //////////
			////////                            Composition Root and Setup                           //////////
			////////                                                                                 //////////
			///////////////////////////////////////////////////////////////////////////////////////////////////


			var actionPanelViewModel  = new ActionPanelViewModel();
			var displayPanelViewModel = new DisplayPanelViewModel();

			var mainWindowViewModel = new MainWindowViewModel(actionPanelViewModel,
						displayPanelViewModel);
			var mainWindow = new MainWindow
			{
				DataContext = mainWindowViewModel
			};

			mainWindow.ShowDialog();
		}
	}
}
