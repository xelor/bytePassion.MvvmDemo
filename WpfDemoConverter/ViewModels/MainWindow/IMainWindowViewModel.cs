﻿using System.Windows.Input;
using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemo2.ViewModels.MainWindow
{
	internal interface IMainWindowViewModel : IViewModel
	{
		ICommand ShowLabel { get; }
		ICommand HideLabel { get; }	

		bool IsLabelVisible { get; }
		string LabelText { get; }
	}
}