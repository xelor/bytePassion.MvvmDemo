﻿using System.ComponentModel;
using System.Windows.Input;

#pragma warning disable 0067

namespace WpfDemo2.ViewModels.MainWindow
{
	internal class MainWindowViewModelSampleData : IMainWindowViewModel
	{
		public MainWindowViewModelSampleData()
		{
			IsLabelVisible = true;
			LabelText = "SampleText";
		}

		public ICommand ShowLabel => null;
		public ICommand HideLabel => null;

		public bool IsLabelVisible { get; }
		public string LabelText { get; }

		public void Dispose () { }
		public event PropertyChangedEventHandler PropertyChanged;		
	}
}