﻿using System.ComponentModel;
using System.Windows.Input;
using bytePassion.Lib.FrameworkExtensions;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemo2.ViewModels.MainWindow
{
	internal class MainWindowViewModel : ViewModel, IMainWindowViewModel
	{
		private bool isLabelVisible;

		public MainWindowViewModel()
		{
			LabelText = "SampleText";
			IsLabelVisible = true;

			ShowLabel = new Command(() => IsLabelVisible = true);
			HideLabel = new Command(() => IsLabelVisible = false);			
		}
		 
		public ICommand ShowLabel { get; }
		public ICommand HideLabel { get; }

		public bool IsLabelVisible
		{
			get { return isLabelVisible; }
			private set { PropertyChanged.ChangeAndNotify(this, ref isLabelVisible, value); }
		}

		public string LabelText { get; }

		protected override void CleanUp () { }
		public override event PropertyChangedEventHandler PropertyChanged;
	}
}
