﻿using System.ComponentModel;
using System.Windows.Input;
using bytePassion.Lib.Communication.State;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.Commands.Updater;
using bytePassion.Lib.WpfLib.ViewModelBase;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.ActionPanel
{
	internal class ActionPanelViewModel : ViewModel, IActionPanelViewModel
	{
		private const string TextToSet = "Button was clicked and is not clickable any more";

		public ActionPanelViewModel (ISharedState<string> labelTextVariable)
		{
			SayHalloWorld = new Command(
				() => labelTextVariable.Value = TextToSet,
				() => labelTextVariable.Value != TextToSet,
				new SharedStateCommandUpdate<string>(labelTextVariable)
			);
		}

		public ICommand SayHalloWorld { get; }

		protected override void CleanUp ()
		{
			((Command)SayHalloWorld).Dispose();
		}

		public override event PropertyChangedEventHandler PropertyChanged;
	}

}
