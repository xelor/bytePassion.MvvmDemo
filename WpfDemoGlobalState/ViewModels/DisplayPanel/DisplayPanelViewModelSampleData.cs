﻿using System.ComponentModel;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal class DisplayPanelViewModelSampleData : IDisplayPanelViewModel
	{
		public DisplayPanelViewModelSampleData ()
		{
			LabelText = "SampleText";
		}

		public string LabelText { get; }

		public void Dispose () { }
		public event PropertyChangedEventHandler PropertyChanged;
	}
}