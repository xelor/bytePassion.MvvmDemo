﻿using System.ComponentModel;
using bytePassion.Lib.Communication.State;
using bytePassion.Lib.FrameworkExtensions;
using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal class DisplayPanelViewModel : ViewModel, IDisplayPanelViewModel
	{
		private readonly ISharedStateReadOnly<string> labelTextVariable;

		private string labelText;

		public DisplayPanelViewModel (ISharedStateReadOnly<string> labelTextVariable)
		{
			this.labelTextVariable = labelTextVariable;
			labelTextVariable.StateChanged += OnLabelTextVariableChanged;
			OnLabelTextVariableChanged(labelTextVariable.Value);
		}

		private void OnLabelTextVariableChanged (string newLabelText)
		{
			LabelText = newLabelText;
		}

		public string LabelText
		{
			get { return labelText; }
			private set { PropertyChanged.ChangeAndNotify(this, ref labelText, value); }
		}

		protected override void CleanUp ()
		{
			labelTextVariable.StateChanged -= OnLabelTextVariableChanged;
		}

		public override event PropertyChangedEventHandler PropertyChanged;
	}
}
