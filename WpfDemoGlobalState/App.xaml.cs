﻿using System.Windows;
using bytePassion.Lib.Communication.State;
using WpfDemo.ViewModels.ActionPanel;
using WpfDemo.ViewModels.DisplayPanel;
using WpfDemo.ViewModels.MainWindow;

namespace WpfDemo
{
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			///////////////////////////////////////////////////////////////////////////////////////////////////
			////////                                                                                 //////////
			////////                            Composition Root and Setup                           //////////
			////////                                                                                 //////////
			///////////////////////////////////////////////////////////////////////////////////////////////////


			const string initialLabelText = "nothing done yet";

			var textLabelVariable = new SharedState<string>(initialLabelText);

			var actionPanelViewModel  = new ActionPanelViewModel(textLabelVariable);
			var displayPanelViewModel = new DisplayPanelViewModel(textLabelVariable);
			
			var mainWindowViewModel = new MainWindowViewModel(actionPanelViewModel,
															  displayPanelViewModel);
			var mainWindow = new MainWindow
			{
				DataContext = mainWindowViewModel
			};

			mainWindow.ShowDialog();

		}
	}
}
