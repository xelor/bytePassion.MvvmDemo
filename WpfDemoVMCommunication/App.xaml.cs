﻿using System.Windows;
using bytePassion.Lib.Communication.MessageBus;
using bytePassion.Lib.Communication.MessageBus.HandlerCollection;
using bytePassion.Lib.Communication.ViewModel;
using bytePassion.Lib.Communication.ViewModel.Messages;
using WpfDemo.ViewModelMessages;
using WpfDemo.ViewModels.ActionPanel;
using WpfDemo.ViewModels.DisplayPanel;
using WpfDemo.ViewModels.MainWindow;

namespace WpfDemo
{
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			///////////////////////////////////////////////////////////////////////////////////////////////////
			////////                                                                                 //////////
			////////                            Composition Root and Setup                           //////////
			////////                                                                                 //////////
			///////////////////////////////////////////////////////////////////////////////////////////////////

			var handlerCollection = new SingleHandlerCollection<ViewModelMessage>();
			var viewModelMessageBus = new LocalMessageBus<ViewModelMessage>(handlerCollection);
			var viewModelCollections = new ViewModelCollections();
			var viewModelCommunication = new ViewModelCommunication(viewModelMessageBus,
						viewModelCollections);

			var actionPanelViewModel  = new ActionPanelViewModel(viewModelCommunication);
			var displayPanelViewModel = new DisplayPanelViewModel();

			var mainWindowViewModel = new MainWindowViewModel(actionPanelViewModel,
															  displayPanelViewModel);
			var mainWindow = new MainWindow
			{
				DataContext = mainWindowViewModel
			};

			viewModelCommunication.RegisterViewModelMessageHandler<NewLabelText>(displayPanelViewModel);

			mainWindow.ShowDialog();
		}
	}
}
