﻿using bytePassion.Lib.Communication.ViewModel.Messages;

namespace WpfDemo.ViewModelMessages
{
	internal class NewLabelText : ViewModelMessage
	{
		public NewLabelText(string labelText)
		{
			LabelText = labelText;
		}

		public string LabelText { get; }
	}
}
