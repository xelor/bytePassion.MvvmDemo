﻿using System.ComponentModel;
using bytePassion.Lib.FrameworkExtensions;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModelMessages;

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal class DisplayPanelViewModel : ViewModel, IDisplayPanelViewModel
	{
		private const string InitialLabelText = "nothing done yet";
		private string labelText;

		public DisplayPanelViewModel ()
		{
			LabelText = InitialLabelText;
		}

		public void Process (NewLabelText message)
		{
			LabelText = message.LabelText;
		}

		public string LabelText
		{
			get { return labelText; }
			private set { PropertyChanged.ChangeAndNotify(this, ref labelText, value); }
		}

		protected override void CleanUp () { }
		public override event PropertyChangedEventHandler PropertyChanged;
	}
}
