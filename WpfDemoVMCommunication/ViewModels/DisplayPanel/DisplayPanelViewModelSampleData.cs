﻿using System.ComponentModel;
using WpfDemo.ViewModelMessages;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal class DisplayPanelViewModelSampleData : IDisplayPanelViewModel
	{
		public DisplayPanelViewModelSampleData ()
		{
			LabelText = "SampleText";
		}

		public string LabelText { get; }

		public void Process (NewLabelText message) { }
		public void Dispose () { }
		public event PropertyChangedEventHandler PropertyChanged;
		
	}
}