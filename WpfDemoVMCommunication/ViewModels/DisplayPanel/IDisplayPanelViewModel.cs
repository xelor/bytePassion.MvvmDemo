﻿using bytePassion.Lib.Communication.ViewModel.Messages;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModelMessages;

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal interface IDisplayPanelViewModel : IViewModel, 
												IViewModelMessageHandler<NewLabelText>
	{
		string LabelText { get; }
	}
}