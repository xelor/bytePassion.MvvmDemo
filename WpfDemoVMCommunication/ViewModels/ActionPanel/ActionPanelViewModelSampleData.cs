﻿using System.ComponentModel;
using System.Windows.Input;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.ActionPanel
{
	internal class ActionPanelViewModelSampleData : IActionPanelViewModel
	{
		public ICommand SayHalloWorld => null;

		public event PropertyChangedEventHandler PropertyChanged;
		public void Dispose () { }
	}
}