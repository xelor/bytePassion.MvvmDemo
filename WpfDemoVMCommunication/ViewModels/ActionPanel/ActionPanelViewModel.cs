﻿using System.ComponentModel;
using System.Windows.Input;
using bytePassion.Lib.Communication.ViewModel;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModelMessages;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.ActionPanel
{
	internal class ActionPanelViewModel : ViewModel, IActionPanelViewModel
	{
		private readonly IViewModelCommunication viewModelCommunication;
		private const string TextToSet = "Button was clicked and is not clickable any more";
		private bool wasButtonClicked;

		public ActionPanelViewModel (IViewModelCommunication viewModelCommunication)
		{
			this.viewModelCommunication = viewModelCommunication;
			wasButtonClicked = false;

			SayHalloWorld = new Command(DoSayHalloWorld,
					  () => !wasButtonClicked);
		}

		private void DoSayHalloWorld ()
		{
			viewModelCommunication.Send(new NewLabelText(TextToSet));
			wasButtonClicked = true;
			((Command)SayHalloWorld).RaiseCanExecuteChanged();
		}

		public ICommand SayHalloWorld { get; }

		protected override void CleanUp () { }
		public override event PropertyChangedEventHandler PropertyChanged;
	}
}
