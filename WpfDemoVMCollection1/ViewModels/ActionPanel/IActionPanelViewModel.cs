using System.Windows.Input;
using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemo.ViewModels.ActionPanel
{
	internal interface IActionPanelViewModel : IViewModel
	{
		ICommand SendTextToItem2 { get; }
		ICommand SendTextToItem3 { get; }
		ICommand SendTextToItem5 { get; }
	}
}
