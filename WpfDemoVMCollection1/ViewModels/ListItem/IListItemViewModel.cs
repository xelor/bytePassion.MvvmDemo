﻿using bytePassion.Lib.Communication.ViewModel;
using bytePassion.Lib.Communication.ViewModel.Messages;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModelMessages;

namespace WpfDemo.ViewModels.ListItem
{
	internal interface IListItemViewModel : IViewModel, 
											IViewModelCollectionItem<int>,
											IViewModelMessageHandler<NewLabelText>
	{
		string LabelText { get; }
	}
}
