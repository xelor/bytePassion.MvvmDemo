﻿using System.ComponentModel;
using bytePassion.Lib.Communication.ViewModel;
using bytePassion.Lib.FrameworkExtensions;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.Global;
using WpfDemo.ViewModelMessages;

namespace WpfDemo.ViewModels.ListItem
{
	internal class ListItemViewModel : ViewModel, IListItemViewModel
	{
		private readonly IViewModelCommunication viewModelCommunication;
		private string labelText;

		public ListItemViewModel(IViewModelCommunication viewModelCommunication,
							     int identifier)
		{
			this.viewModelCommunication = viewModelCommunication;
			Identifier = identifier;

			LabelText = "nothing done yet";

			viewModelCommunication.RegisterViewModelAtCollection<IListItemViewModel, int>(
				Constants.ListItemCollection,
				this
			);
		}

		public int Identifier { get; }

		public string LabelText
		{
			get { return labelText; }
			private set { PropertyChanged.ChangeAndNotify(this, ref labelText, value); }
		}

		public void Process(NewLabelText message)
		{
			LabelText = message.LabelText;
		}

		protected override void CleanUp()
		{
			viewModelCommunication.DeregisterViewModelAtCollection<IListItemViewModel, int>(
				Constants.ListItemCollection,
				this
				);
		}

		public override event PropertyChangedEventHandler PropertyChanged;


		
	}
}