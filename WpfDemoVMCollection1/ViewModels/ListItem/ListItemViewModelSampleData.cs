﻿using System.ComponentModel;
using WpfDemo.ViewModelMessages;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.ListItem
{
	internal class ListItemViewModelSampleData : IListItemViewModel
	{
		public ListItemViewModelSampleData()
		{
			Identifier = 1;
			LabelText = "SampleText";
		}

		public int Identifier { get; }
		public string LabelText { get; }

		public void Process (NewLabelText message) { }

		public void Dispose () { }
		public event PropertyChangedEventHandler PropertyChanged;				
	}
}