﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using WpfDemo.ViewModels.ListItem;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal class DisplayPanelViewModelSampleData : IDisplayPanelViewModel
	{
		public DisplayPanelViewModelSampleData()
		{
			ListItems = new ObservableCollection<IListItemViewModel>
			{
				new ListItemViewModelSampleData(),
				new ListItemViewModelSampleData(),
				new ListItemViewModelSampleData(),
				new ListItemViewModelSampleData()
			};
		}

		public ObservableCollection<IListItemViewModel> ListItems { get; }

		public void Dispose () { }
		public event PropertyChangedEventHandler PropertyChanged;		
	}
}