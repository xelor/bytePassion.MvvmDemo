﻿using System.ComponentModel;
using WpfDemo.ViewModels.ActionPanel;
using WpfDemo.ViewModels.DisplayPanel;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.MainWindow
{
	internal class MainWindowViewModelSampleData : IMainWindowViewModel
	{
		public MainWindowViewModelSampleData()
		{
			ActionPanelViewModel  = new ActionPanelViewModelSampleData();
			DisplayPanelViewModel = new DisplayPanelViewModelSampleData();	
		}

		public IActionPanelViewModel  ActionPanelViewModel  { get; }
		public IDisplayPanelViewModel DisplayPanelViewModel { get; }

		public void Dispose () { }
		public event PropertyChangedEventHandler PropertyChanged;		
	}
}