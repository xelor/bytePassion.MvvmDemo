﻿using System.ComponentModel;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModels.ActionPanel;
using WpfDemo.ViewModels.DisplayPanel;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.MainWindow
{
	internal class MainWindowViewModel : ViewModel, IMainWindowViewModel
	{

		public MainWindowViewModel(IActionPanelViewModel actionPanelViewModel,
								   IDisplayPanelViewModel displayPanelViewModel)
		{
			ActionPanelViewModel  = actionPanelViewModel;
			DisplayPanelViewModel = displayPanelViewModel;
		}

		public IActionPanelViewModel  ActionPanelViewModel  { get; }
		public IDisplayPanelViewModel DisplayPanelViewModel { get; }

		protected override void CleanUp(){}
		public override event PropertyChangedEventHandler PropertyChanged;
	}
}
