﻿using System.ComponentModel;
using System.Windows.Input;

#pragma warning disable 0067

namespace WpfDemoHalloWorld.ViewModels.MainWindow
{
	internal class MainWindowViewModelSampleData : IMainWindowViewModel
	{
		public ICommand SayHalloWorld => null;

		public void Dispose () { }
		public event PropertyChangedEventHandler PropertyChanged;				
	}
}