﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.ViewModelBase;

#pragma warning disable 0067

namespace WpfDemoHalloWorld.ViewModels.MainWindow
{
	internal class MainWindowViewModel : ViewModel, IMainWindowViewModel
	{
		private bool buttonWasClicked;

		public MainWindowViewModel ()
		{
			buttonWasClicked = false;

			SayHalloWorld = new ParameterrizedCommand<double>(
				DoSayHalloWorld,
				_ => !buttonWasClicked
			);
		}

		private void DoSayHalloWorld (double windowWidth)
		{
			MessageBox.Show($"fensterbreite: {windowWidth}");
			buttonWasClicked = true;
			((ParameterrizedCommand<double>)SayHalloWorld).RaiseCanExecuteChanged();
		}

		public ICommand SayHalloWorld { get; }

		protected override void CleanUp() {	}
		public override event PropertyChangedEventHandler PropertyChanged;		
	}
}
