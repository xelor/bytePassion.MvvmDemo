﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.ViewModelBase;

#pragma warning disable 0067

namespace WpfDemoHalloWorld.ViewModels.MainWindow
{
	internal class MainWindowViewModel : ViewModel, IMainWindowViewModel
	{
		public MainWindowViewModel ()
		{
			SayHalloWorld = new ParameterrizedCommand<double>(DoSayHalloWorld);
		}

		private void DoSayHalloWorld (double windowWidth)
		{
			MessageBox.Show($"fensterbreite: {windowWidth}");
		}

		public ICommand SayHalloWorld { get; }

		protected override void CleanUp() {	}
		public override event PropertyChangedEventHandler PropertyChanged;		
	}
}
