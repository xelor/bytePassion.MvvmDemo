﻿using System.Windows;
using WpfDemoHalloWorld.ViewModels.MainWindow;

namespace WpfDemoHalloWorld
{
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			//////////////////////////////////////////////////////////////////////
			////////                                                       ///////			
			////////               Composition Root and Setup              ///////			
			////////                                                       ///////			
			//////////////////////////////////////////////////////////////////////

			var mainWindowViewModel = new MainWindowViewModel();

			var mainWindow = new MainWindow
			{
				DataContext = mainWindowViewModel
			};

			mainWindow.ShowDialog();
		}
	}
}
