﻿using System.ComponentModel;
using System.Windows.Input;
using bytePassion.Lib.FrameworkExtensions;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.Commands.Updater;
using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemoHalloWorld.ViewModels.MainWindow
{
	internal class MainWindowViewModel : ViewModel, IMainWindowViewModel
	{
		private const string InitialLabelText = "nothing done yet";
		private string labelText;

		public MainWindowViewModel ()
		{
			LabelText = InitialLabelText;

			SayHalloWorld = new Command(
				DoSayHalloWorld,
				() => LabelText == InitialLabelText,
				new PropertyChangedCommandUpdater(this, nameof(LabelText))
			);
		}

		private void DoSayHalloWorld ()
		{
			LabelText = "Button was clicked and is not clickable any more";
		}

		public ICommand SayHalloWorld { get; }

		public string LabelText
		{
			get { return labelText; }
			private set { PropertyChanged.ChangeAndNotify(this, ref labelText, value); }
		}

		protected override void CleanUp ()
		{
			((Command)SayHalloWorld).Dispose();
		}

		public override event PropertyChangedEventHandler PropertyChanged;		
	}
}
