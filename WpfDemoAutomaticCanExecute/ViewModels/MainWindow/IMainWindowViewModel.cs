﻿using System.Windows.Input;
using bytePassion.Lib.WpfLib.ViewModelBase;

namespace WpfDemoHalloWorld.ViewModels.MainWindow
{
	internal interface IMainWindowViewModel : IViewModel
	{
		ICommand SayHalloWorld { get; }
		string   LabelText     { get; }
	}
}