﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using bytePassion.Lib.Communication.ViewModel;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModels.ListItem;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal class DisplayPanelViewModel : ViewModel, IDisplayPanelViewModel
	{		
		public DisplayPanelViewModel(IViewModelCommunication viewModelCommunication)
		{
			ListItems = new ObservableCollection<IListItemViewModel>
			{
				new ListItemViewModel(viewModelCommunication, 1),
				new ListItemViewModel(viewModelCommunication, 2),
				new ListItemViewModel(viewModelCommunication, 3),
				new ListItemViewModel(viewModelCommunication, 4),
				new ListItemViewModel(viewModelCommunication, 5),
				new ListItemViewModel(viewModelCommunication, 6)
			};
		}

		public ObservableCollection<IListItemViewModel> ListItems { get; }

		protected override void CleanUp() {	}
		public override event PropertyChangedEventHandler PropertyChanged;		
	}
}
