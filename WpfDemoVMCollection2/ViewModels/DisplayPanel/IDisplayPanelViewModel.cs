﻿using System.Collections.ObjectModel;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.ViewModels.ListItem;

namespace WpfDemo.ViewModels.DisplayPanel
{
	internal interface IDisplayPanelViewModel : IViewModel
	{
		ObservableCollection<IListItemViewModel> ListItems { get; }
	}
}