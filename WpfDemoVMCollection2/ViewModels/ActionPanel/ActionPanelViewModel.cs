﻿using System.ComponentModel;
using System.Windows.Input;
using bytePassion.Lib.Communication.ViewModel;
using bytePassion.Lib.WpfLib.Commands;
using bytePassion.Lib.WpfLib.ViewModelBase;
using WpfDemo.Global;
using WpfDemo.ViewModelMessages;

#pragma warning disable 0067

namespace WpfDemo.ViewModels.ActionPanel
{
	internal class ActionPanelViewModel : ViewModel, IActionPanelViewModel
	{
		private readonly IViewModelCommunication viewModelCommunication;		

		public ActionPanelViewModel(IViewModelCommunication viewModelCommunication)
		{
			this.viewModelCommunication = viewModelCommunication;
			
			SendTextToAll   = new Command(SendToAllItems);
			SendTextToItem3 = new Command(() => SendToItem(3));
			SendTextToItem5 = new Command(() => SendToItem(5));
		}

		private void SendToItem (int itemKey)
		{
			viewModelCommunication.SendTo(
				Constants.ListItemCollection,
				itemKey,
				new NewLabelText($"hallo to {itemKey}")	
			);
		}

		private void SendToAllItems()
		{
			viewModelCommunication.SendToCollection<int, NewLabelText>(
				Constants.ListItemCollection,	
				new NewLabelText("hallo to all")
			);
		}	
		
		public ICommand SendTextToAll   { get; }
		public ICommand SendTextToItem3 { get; }
		public ICommand SendTextToItem5 { get; }

		protected override void CleanUp() {	}
		public override event PropertyChangedEventHandler PropertyChanged;		
	}
}
